// Program: Payment Gateway
// Email: husnifahmi@outlook.com
// Date: 2022-12-30
// 3 DUKPT Key Management Scheme
// Derived Unique Key Per Transaction (DUKPT) is a key management scheme
// in which for every transaction, a unique key is used which is derived
// from a fixed key. Therefore, if a derived key is compromised, future
// and past transaction data are still protected since the next or prior
// keys cannot be determined easily. DUKPT is specified in ANSI X9.24 part 1
//
//
//
// 3.1 In Brief
// • The transaction request from POS terminal to payment processing gateway needs to be encrypted.
// • Of course! How is the encryption done?
// 		By using the well known Triple DES crypto algorithm. And each transaction will have unique
// 		key.
// • Unique? But how are the keys to algorithm known? Are they loaded in the terminal and gateway
// beforehand?
// 		No. Key is generated dynamically with each transaction. The terminal while doing the current
// 		transaction will generate the next one and store it. The key for the current transaction is
// 		cryptographically changed to the key for the next transaction.
// • Ok. But how does the gateway know about it and how does it decrypt?
// 		Gateway would know because, it has the mother of all keys. The Super-secret key. When the
// 		client sends the encrypted payload it also sends some meta-data (unencrypted). With the metadata and the Super-secret key the gateway would cryptographically generate the same key that
// 		terminal used for the encryption.
// • Great! What is this mechanism called?
// 		DUKPT - Derived Unique Key Per Transaction
//

// https://stackoverflow.com/questions/48124565/why-does-vscode-delete-golang-source-on-save
// https://www.developer.com/languages/intro-socket-programming-go/

package main
import (
    "fmt"
    "net"
    "os"
	dukpt "tle/pgw/dukpt"
)
// go env -w GO111MODULE=off
// go env -w GO111MODULE=on
const (
    SERVER_HOST = "localhost"
    SERVER_PORT = "9988"
    SERVER_TYPE = "tcp"
)
func main() {
	// As a first step in setting up the TG and terminal for the DUKPT, we need
	// to generate a random 128 bit key. This key is called as Base Derivation Key (BDK)
	// and this is the Super-secret key that was mentioned above. This shall then be
	// stored in a Hardware Security Module (HSM). We can generate multiple BDKs and 
	// store them. On receiving a transaction request from the terminal an appropriate
	// BDK shall be chosen (the request will contain information (Meta data) to enable
	// TG to find which is ‘appropriate’). The chosen BDK will be used to generate
	//(eventually) the encryption key that the terminal would have used for encrypting
	// the request.

	// Base Derivation Key (BDK), Super-secret key, stored in HSM
	var testBdk = []byte{0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 
						 0xFE, 0xDC, 0xBA, 0x98, 0x76, 0x54, 0x32, 0x10}

	// Once the BDK is identified, we need to generate what is known as 
	// Initial PIN Encryption Key (IPEK). Another set of inputs for generating
	// IPEK are Key-Set ID and TRSM ID. Key-Set ID uniquely identifies the BDK.
	// TRSM ID is the device ID of the transaction originating terminals. 
	var testIpek = []byte{0x6a, 0xc2, 0x92, 0xfa, 0xa1, 0x31, 0x5b, 0x4d,
						  0x85, 0x8a, 0xb3, 0xa3, 0xd7, 0xd5, 0x93, 0x3a}
	
	// Key Serial Number (Key Name): Key-Set ID, TRSM ID, Transaction Counter; 10 bytes
	// The Key Name shall be 80 bits – 10 Bytes
	// 		Transaction Counter – 21 Bits 
	// 		TRSM ID – 19 Bits 
	// 		Key Set ID – 40 bits (5 bytes) 
	var testKsn = []byte{0xFF, 0xFF, 0x98, 0x76, 0x54, 0x32, 0x10, 0xE0, 0x00, 0x08}

	// Whenever the terminal sends the encrypted ISO -8583 frame, TG also receives
	// along with it the KEY-NAME. From the KEY-NAME the TG shall determine the
	// following: 
	// 		1. From the Key-Set ID TG will determine which BDK to use for this 
	// 		   current transaction (Multiple BDKs can be stored in the HSM) 
	// 		2. From the Key-Set ID, TRSM ID and the chosen BDK, TG shall 
	// 		   cryptographically recreate the IPEK that was injected to terminal
	// 		   in the beginning. 
	// 		3. With the IPEK and the transaction counter value in the Meta data,
	//		   TG shall invoke the Non-reversible transformation. The result is the
	// 		   same key that was used by the client for encrypting this transaction.
	//		   With the key generated thus, TG shall proceed to decrypt 
	//		   the ISO-8583 frame. 

	// 1. Generate BDK. Store it in HSM from which TG can access. 
	// 2. From BDK generate IPEK. Inject IPEK into the terminal in a secure
	//	  and controlled manner.
	// 3. Input the Key-Set ID and TRSM ID (which were used for generating the BDK) 
	// 4. Invoke DUKPT software on the terminal. This should result in Future Key
	// 	  getting generated and stored. 
	// 5. Terminal shall use only TDES for the encryption. 
	// 6. For every transaction that the terminal shall send to TG, it shall send
	// along with the encrypted ISO-8583 frame/EIS frame 10 bytes of Meta Data.

	// Derive IPEK from BDK and KSN
	// Once the BDK is identified, we need to generate what is known as 
	// Initial PIN Encryption Key (IPEK). Another set of inputs for generating
	// IPEK are Key-Set ID and TRSM ID. Key-Set ID uniquely identifies the BDK.
	// TRSM ID is the device ID of the transaction originating terminals. 
	ipek, err := dukpt.DeriveIpekFromBdk(testBdk, testKsn)

	_ = testIpek
	_ = ipek

    fmt.Println("Server Running...")
    server, err := net.Listen(SERVER_TYPE, SERVER_HOST+":"+SERVER_PORT)
    if err != nil {
        fmt.Println("Error listening:", err.Error())
        os.Exit(1)
    }
    defer server.Close()
    fmt.Println("Listening on " + SERVER_HOST + ":" + SERVER_PORT)
    fmt.Println("Waiting for client...")
    for {
        connection, err := server.Accept()
        if err != nil {
            fmt.Println("Error accepting: ", err.Error())
            os.Exit(1)
        }
    	fmt.Println("client connected")
    	go processClient(connection)
    }
}

func processClient(connection net.Conn) {
    buffer := make([]byte, 1024)
    mLen, err := connection.Read(buffer)
    if err != nil {
        fmt.Println("Error reading:", err.Error())
    }
    fmt.Println("Received: ", string(buffer[:mLen]))
    _, err = connection.Write([]byte("Thanks! Got your message:" + string(buffer[:mLen])))
    connection.Close()
}
